# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit git-r3 autotools

DESCRIPTION="Apple Lossless Audio Codec library"
HOMEPAGE="https://github.com/mikebrady/alac"
EGIT_REPO_URI="https://github.com/mikebrady/alac.git"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS=""

src_configure() {
  autoreconf -fi
  econf
}

