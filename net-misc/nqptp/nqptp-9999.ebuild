# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit git-r3 autotools

DESCRIPTION="Not Quite PTP"
HOMEPAGE="https://github.com/mikebrady/nqptp"
EGIT_REPO_URI="https://github.com/mikebrady/nqptp.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
	autoreconf -fi
	econf --with-systemd-startup
}

