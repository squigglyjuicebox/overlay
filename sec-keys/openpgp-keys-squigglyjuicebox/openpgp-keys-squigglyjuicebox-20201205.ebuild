# Copyright 2022 squigglyjuicebox
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="OpenPGP keys used by squigglyjuicebox"
HOMEPAGE="https://lab.jury-online.at/squigglyjuicebox/overlay"

LICENSE="public-domain"
SLOT="0"
KEYWORDS="~alpha amd64 arm arm64 hppa ~ia64 ~m68k ~mips ppc ppc64 ~riscv s390 sparc x86"

S="${FILESDIR}"

src_install() {
  insinto "/usr/share/openpgp-keys"
  newins "squigglyjuicebox-${PV}.asc" "squigglyjuicebox.asc"
}
