# Copyright 2020 squigglyjuicebox
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit git-r3 linux-mod

DESCRIPTION="Linux kernel vendor specific hardware reset module for sequences that are too complex/complicated to land in pci_quirks.c"
HOMEPAGE="https://github.com/gnif/vendor-reset"
EGIT_REPO_URI="https://github.com/gnif/vendor-reset.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"

CONFIG_CHECK="FTRACE
              FUNCTION_TRACER
              ~KPROBES
              ~PCI_QUIRKS
              ~KALLSYMS
              ~KALLSYMS_ALL"
MODULE_NAMES="vendor-reset(kernel/drivers/misc:${S}:${S})"
BUILD_TARGETS="all"
