# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
inherit git-r3 autotools

DESCRIPTION="AirPlay and AirPlay 2 audio player"
HOMEPAGE="https://github.com/mikebrady/shairport-sync"
EGIT_REPO_URI="https://github.com/mikebrady/shairport-sync.git"

LICENSE="MIT"
SLOT="0"
KEYWORDS=""

IUSE="
  +airplay-2
  +alsa pulseaudio pipewire jack soundio stdout pipe
  +soxr +alac convolution
  +metadata
  libdaemon systemd systemv
  +openssl mbedtls
  +avahi dns-sd
  sample
"

REQUIRED_USE="
  || ( alsa pulseaudio pipewire jack soundio stdout pipe )
  ^^ ( openssl mbedtls )
  airplay-2? ( openssl avahi )
"

RDEPEND="
  dev-libs/libconfig
  alsa? ( media-libs/alsa-lib )
  pulseaudio? ( media-sound/pulseaudio )
  pipewire? ( media-video/pipewire )
  jack? ( virtual/jack )
  soundio? ( media-libs/libsoundio )
  soxr? ( media-libs/soxr )
  alac? ( media-libs/alac )
  convolution? ( media-libs/libsndfile )
  dev-libs/libdaemon
  openssl? ( dev-libs/openssl )
  mbedtls? ( net-libs/mbedtls )
  net-dns/avahi
  dev-libs/popt
  airplay-2? (
    media-video/ffmpeg 
    app-pda/libplist
    dev-libs/libsodium
    dev-libs/libgcrypt
    sys-apps/util-linux
    app-editors/vim-core
    net-misc/nqptp
  )
"

DEPEND="${RDEPEND}"

src_configure() {
  sed -i "/useradd/d" Makefile.am
  sed -i "/groupadd/d" Makefile.am

  autoreconf -fi
  local myconf
  if use openssl; then
    myconf="${myconf} --with-ssl=openssl"
  elif use mbedtls; then
    myconf="${myconf} --with-ssl=mbedtls"
  fi
  econf \
    --with-os=linux \
    --sysconfdir=/etc \
    $(use_with airplay-2) \
    $(use_with alsa) \
    $(use_with pulseaudio pa) \
    $(use_with pipewire pw) \
    $(use_with jack) \
    $(use_with soundio) \
    $(use_with stdout) \
    $(use_with pipe) \
    $(use_with soxr) \
    $(use_with alac apple-alac) \
    $(use_with convolution) \
    $(use_with metadata) \
    $(use_with libdaemon) \
    $(use_with systemd) \
    $(use_with systemv) \
    $(use_with avahi) \
    $(use_with dns-sd dns_sd) \
    $(use_with sample configfiles) \
    ${myconf}
}

src_install() {
  default

  if use systemd; then
    insinto "/usr/lib/sysusers.d"
    newins "${FILESDIR}/${PN}.sysusers" "${PN}.conf"
  fi
}

pkg_postinst() {
  einfo "You'll need to open the following ports:"
  einfo "  For AirPlay 1 and 2:"
  einfo "    TCP 3689"
  einfo "    TCP 5000"
  einfo "    TCP/UDP 5353"
  einfo "    UDP 6000:6010"
  if use airplay-2; then
    einfo "  For AirPlay 2:"
    einfo "    UDP 319:320"
    einfo "    TCP 7000"
    einfo "    TCP/UDP 32768:61000"
  fi

  if use systemd; then
    einfo "To enable the service, run:"
    einfo "  systemctl enable shairport-sync"
  fi
}

