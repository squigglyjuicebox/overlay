# Overlay Installation

* Create the file `/etc/portage/repos.conf/squigglyjuicebox.conf` with the following contents:
```ini
[squigglyjuicebox]
location = /var/db/repos/squigglyjuicebox
sync-type = git
sync-uri = https://gitlab.com/squigglyjuicebox/overlay.git
```

* Download the repository with this command:
```console
root # emaint sync -r squigglyjuicebox
```

## OpenPGP Signature Verification

To verify on sync that the top commit in this repository is signed with my keys:

* After installing this overlay, install my signing keys:
```console
root # emerge sec-keys/openpgp-keys-squigglyjuicebox
```

* Add these lines to the bottom of `/etc/portage/repos.conf/squigglyjuicebox.conf`:
```ini
sync-openpgp-key-path = /usr/share/openpgp-keys/squigglyjuicebox.asc
sync-openpgp-keyserver = hkps://keys.openpgp.org
sync-git-verify-commit-signature = true
```
